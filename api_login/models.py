from django.db import models


class User(models.Model):
    email = models.CharField(max_length=50)
    password = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'api_user_user'


class Book(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'book'
        permissions = (("can_mark_returned", "Set book as returned"),)

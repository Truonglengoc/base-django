
from api_login import views as login_view
from django.urls import path

urlpatterns = [
    path('login/', login_view.Login().as_view(), name="login"),
    path('registration/', login_view.Registration.as_view()),
    path('check_authentication/', login_view.CheckAuthentication.as_view()),
    path('get_book/', login_view.BookAPI.as_view())
]

from django.shortcuts import render
from rest_framework import permissions
from rest_framework.authentication import BasicAuthentication, TokenAuthentication, SessionAuthentication
from django.core.exceptions import PermissionDenied

from api_login import schema, models
from django.http import HttpResponse
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User

from myproject.authentication import BearerAuthentication

def permission_required(permission_name, raise_exception=False):
    class PermissionRequired(permissions.BasePermission):
        def has_permission(self, request, view):
            user = request.user
            if user.has_perm(permission_name):
                return True
            if raise_exception:
                raise PermissionDenied("Don't have permission")
            return False

    return PermissionRequired

class Login(APIView):
    template_name = 'admin/login.html'
    permission_classes = [AllowAny, ]

    def get(self, request):
        return render(
            request,
            self.template_name,
            {})

    def post(self, request):
        first_person = User.objects.filter(username="admin").first()

        token = Token.objects.create(user=first_person)
        data = {
            'user': first_person,
            'token': token
        }
        _resp = {
            "data": data,
            "meta": {
                "message": "Success",
                "status": 200
            }
        }
        _resp = schema.LoginResp(_resp).data
        return Response(data=_resp, status=200)


class Registration(APIView):
    permission_classes = [AllowAny, ]

    def post(self, request):
        valid_data = schema.RegistrationReq(request.data).data
        user = User.objects.find(username=valid_data.get('username'))
        if user:
            _resp = {
                'message': "Username already exists",
                'status': 409
            }
            _resp = schema.Meta(_resp).data
            return Response(data=_resp, status=409)
        User.objects.create_user(
            last_name=valid_data.get("last_name"),
            first_name=valid_data.get("first_name"),
            password=valid_data.get("password"),
            email=valid_data.get("email"),
            username=valid_data.get("username"),
        )
        _resp = {
            'message': "Success",
            'status': 201
        }
        _resp = schema.Meta(_resp).data
        return Response(data=_resp, status=200)


class CheckAuthentication(APIView):
    authentication_classes = [BearerAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        return Response(data={}, status=200)


class BookAPI(APIView):
    permission_classes = [permission_required('can_mark_returned'),]
    def get(self, request):
        book = models.Book.objects.all()
        _resp = {
            'data': book,
            'meta': {
                'message': "Success",
                'status': 200
            }
        }
        _resp = schema.Meta(_resp).data
        return Response(data=_resp, status=200)

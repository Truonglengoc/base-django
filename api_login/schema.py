from rest_marshmallow import Schema, fields


class Meta(Schema):
    status = fields.Str(required=True)
    message = fields.Str()


class User(Schema):
    email = fields.Str()


class Login(Schema):
    user = fields.Nested(User)
    token = fields.Str()


class LoginResp(Schema):
    meta = fields.Nested(Meta)
    data = fields.Nested(Login)


class RegistrationReq(Schema):
    first_name = fields.Str()
    last_name = fields.Str()
    password = fields.Str(required=True)
    email = fields.Str(required=True)
    username = fields.Str(required=True)


class RegistrationResp(Schema):
    resp = fields.Nested(Meta)


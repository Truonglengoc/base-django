from marshmallow import Schema
from rest_framework import serializers


class MarshValidationError:
    pass


def validate_data(schema: Schema, request_data: dict) -> dict:
    try:
        valid_data = schema.load(request_data)
    except MarshValidationError as err:
        msg = (f"Missing data or invalid field: "
               f"{str(', '.join(err.messages.keys()))}")
        raise serializers.ValidationError({"Error": "True", "Message": msg})
    return valid_data